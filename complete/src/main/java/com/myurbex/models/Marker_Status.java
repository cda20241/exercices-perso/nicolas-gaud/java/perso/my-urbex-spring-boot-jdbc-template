package com.myurbex.models;

import jakarta.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "marker_status")
public class Marker_Status {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	@Basic
	@Column(name = "label")
	private String label;
	@Basic
	@Column(name = "code_status")
	private String code_status;
	@Transient
	private int count;
	
	public Marker_Status() {
		super();
	}
	
	public Marker_Status(int id, String label, String code_status) {
		super();
		this.id = id;
		this.label = label;
		this.code_status = code_status;
	}


	public Marker_Status(int id, String label, String code_status, int count) {
		super();
		this.id = id;
		this.label = label;
		this.code_status = code_status;
		this.count = count;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getCode_status() {
		return code_status;
	}

	public void setCode_status(String code_status) {
		this.code_status = code_status;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	

}
