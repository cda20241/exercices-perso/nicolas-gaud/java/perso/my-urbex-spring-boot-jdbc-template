package com.myurbex.models;

import jakarta.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "marker_motif")
public class Marker_Motif {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	@Basic
	@Column(name = "label")
	private String label;
	@Basic
	@Column(name = "code_motif")
	private String code_motif;
	
	public Marker_Motif() {
		super();
	}
	
	public Marker_Motif(int id, String label, String code_motif) {
		super();
		this.id = id;
		this.label = label;
		this.code_motif = code_motif;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getCode_motif() {
		return code_motif;
	}
	
	public void setCode_motif(String code_motif) {
		this.code_motif = code_motif;
	}
	
	

}
