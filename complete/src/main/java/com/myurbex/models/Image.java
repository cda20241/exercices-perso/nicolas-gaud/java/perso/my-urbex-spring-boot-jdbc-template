package com.myurbex.models;

import java.util.Date;
import jakarta.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "image")
public class Image {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	@Basic
	@Column(name = "name")
	private String name;
	@Basic
	@Column(name = "date_taken")
	private Date date_taken;
	@Basic
	@Column(name = "is_front")
	private boolean is_front;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "marker_id", nullable = false)
	private Marker marker;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = true)
	private User user;
	@Basic
	@Column(name = "image_type_id")
	private int image_type_id;
	@Transient
	private String user_nickname;
	
	public Image() {
		super();
	}
	
	public Image(int id, String name, Date date_taken, boolean is_front, Marker marker, User user, int image_type_id, String user_nickname) {
		super();
		this.id = id;
		this.name = name;
		this.date_taken = date_taken;
		this.is_front = is_front;
		this.marker = marker;
		this.user = user;
		this.image_type_id = image_type_id;
		this.user_nickname = user_nickname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate_taken() {
		return date_taken;
	}

	public void setDate_taken(Date date_taken) {
		this.date_taken = date_taken;
	}
	
	public boolean isIs_front() {
		return is_front;
	}

	public void setIs_front(boolean is_front) {
		this.is_front = is_front;
	}

	public Marker getMarker() {	return marker; }

	public void setMarker(Marker marker) {	this.marker = marker;}

	public User getUser() { return user; }

	public void setUser(User user) { this.user = user; }

	public int getImage_type_id() {
		return image_type_id;
	}

	public void setImage_type_id(int image_type_id) {
		this.image_type_id = image_type_id;
	}

	public String getUser_nickname() {
		return user_nickname;
	}

	public void setUser_nickname(String user_nickname) {
		this.user_nickname = user_nickname;
	}

	

}
