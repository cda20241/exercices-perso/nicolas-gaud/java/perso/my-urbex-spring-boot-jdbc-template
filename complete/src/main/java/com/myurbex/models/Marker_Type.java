package com.myurbex.models;

import jakarta.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "marker_type")
public class Marker_Type {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	@Basic
	@Column(name = "label")
	private String label;
	@Basic
	@Column(name = "code_type")
	private String code_type;
	@Transient
	private int count;
	
	public Marker_Type() {
		super();
	}
	
	public Marker_Type(int id, String label, String code_type) {
		super();
		this.id = id;
		this.label = label;
		this.code_type = code_type;
	}

	public Marker_Type(int id, String label, String code_type, int count) {
		super();
		this.id = id;
		this.label = label;
		this.code_type = code_type;
		this.count = count;
	}
	
	@Override
	public String toString() {
		return label + " (" + count + ")";
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getCode_type() {
		return code_type;
	}
	public void setCode_type(String code_type) {
		this.code_type = code_type;
	}
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	

}
