package com.myurbex.models;

import java.util.ArrayList;
import java.util.Date;
import jakarta.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "marker")
public class Marker {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	@Basic
	@Column(name = "name")
	private String name;
	@Basic
	@Column(name = "description")
	private String description;
	@Transient
	private float latitude;
	@Transient
	private float longitude;
	@Basic
	@Column(name = "is_urbex")
	private boolean is_urbex;
	@Basic
	@Column(name = "maps_link")
	private String maps_link;
	@Transient
	private String type_label;
	@Transient
	private String type_code;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "marker_type_id", nullable = true)
	private Marker_Type marker_type;
	@Transient
	private String status_label;
	@Transient
	private String status_code;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "marker_status_id", nullable = true)
	private Marker_Status marker_status;
	@Transient
	private String motif_label;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "motif_id", nullable = true)
	private Marker_Motif marker_motif;
	@Basic
	@Column(name = "level_id", nullable = true)
	private int level_id ;
	@Transient
	private String nickname;
	@Transient
	private String department_name;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "department_id", nullable = true)
	private Department department;
	@Transient
	private String region_name;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "region_id", nullable = true)
	private Region region;
	@Transient
	private String country_name;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "country_id", nullable = true)
	private Country country;
	@Transient
	private String first_image;
	@Transient
	private Date date_visited;
	@Transient
	private ArrayList<Image> images;
	@Transient
	private int count;	

	public Marker() {
		super();		
	}
	
	public Marker(boolean is_urbex, int count) {
		super();
		this.is_urbex = is_urbex;
		this.count = count;
	}
	
	public Marker(String nickname, int count) {
		super();
		this.nickname = nickname;
		this.count = count;
	}

	public Marker(int id, String name, String description, float latitude, float longitude, 
			boolean is_urbex, String maps_link, String type_label, String type_code, String status_label, String status_code,
			String motif_label, String nickname, String department_name, Department department, String region_name, Region region,
			String country_name, Country country) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.latitude = latitude;
		this.longitude = longitude;
		this.is_urbex = is_urbex;
		this.maps_link = maps_link;
		this.type_label = type_label;
		this.type_code = type_code;
		this.status_label = status_label;
		this.status_code = status_code;
		this.motif_label = motif_label;
		this.nickname = nickname;
		this.department_name = department_name;
		this.department = department;
		this.region_name = region_name;
		this.region = region;
		this.country_name = country_name;
		this.country = country;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public boolean isIs_urbex() {
		return is_urbex;
	}

	public void setIs_urbex(boolean is_urbex) {
		this.is_urbex = is_urbex;
	}
	
	public String getMaps_link() {
		return maps_link;
	}

	public void setMaps_link(String maps_link) {
		this.maps_link = maps_link;
	}

	public String getType_label() {
		return type_label;
	}

	public void setType_label(String type_label) {
		this.type_label = type_label;
	}

	public String getType_code() {
		return type_code;
	}

	public void setType_code(String type_code) {
		this.type_code = type_code;
	}

	public String getStatus_label() {
		return status_label;
	}

	public void setStatus_label(String status_label) {
		this.status_label = status_label;
	}

	public String getStatus_code() {
		return status_code;
	}

	public void setStatus_code(String status_code) {
		this.status_code = status_code;
	}

	public String getMotif_label() {
		return motif_label;
	}

	public void setMotif_label(String motif_label) {
		this.motif_label = motif_label;
	}
	
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}


	public String getDepartment_name() {
		return department_name;
	}

	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getRegion_name() {
		return region_name;
	}

	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public String getCountry_name() {
		return country_name;
	}

	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getFirst_image() {
		return first_image;
	}

	public void setFirst_image(String first_image) {
		this.first_image = first_image;
	}
	
	public Date getDate_visited() {
		return date_visited;
	}

	public void setDate_visited(Date date_visited) {
		this.date_visited = date_visited;
	}

	public ArrayList<Image> getImages() {
		return images;
	}

	public void setImages(ArrayList<Image> images) {
		this.images = images;
	}
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Marker_Type getMarker_type() {
		return marker_type;
	}

	public void setMarker_type(Marker_Type marker_type) {
		this.marker_type = marker_type;
	}

	public Marker_Status getMarker_status() {
		return marker_status;
	}

	public void setMarker_status(Marker_Status marker_status) {
		this.marker_status = marker_status;
	}

	public Marker_Motif getMarker_motif() {
		return marker_motif;
	}

	public void setMarker_motif(Marker_Motif marker_motif) {
		this.marker_motif = marker_motif;
	}

}