package com.myurbex.models;

import jakarta.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "department")
public class Department {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private String id;
	@Basic
	@Column(name = "name")
	private String name;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "region_id", nullable = true)
	private Region region;
	@Transient
	private int count;
	
	public Department() {
		super();
	}
	
	public Department(String id, String name, Region region) {
		super();
		this.id = id;
		this.name = name;
		this.region = region;
	}

	public Department(String id, String name, Region region, int count) {
		super();
		this.id = id;
		this.name = name;
		this.region = region;
		this.count = count;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Region getRegion() {
		return this.region;
	}

	public void setRegion(int region_id) {
		this.region = region;
	}
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
}
