package com.myurbex.models;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.List;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface MarkerRepository extends JpaRepository<User, Integer> {
    ArrayList<Marker> findMarkersByLevelId(int levelId);

    @Override
    long count();

    int countMarkersInIds(List<Integer> ids);
}