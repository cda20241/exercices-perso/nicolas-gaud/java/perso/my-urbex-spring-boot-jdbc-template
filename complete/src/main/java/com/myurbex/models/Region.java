package com.myurbex.models;

import jakarta.persistence.*;
@Entity // This tells Hibernate to make a table out of this class
@Table(name = "region")
public class Region {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	@Basic
	@Column(name = "name")
	private String name;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "country_id", nullable = false)
	private Country country;
	@Transient
	private int count;
	
	public Region() {
		super();
	}
	
	public Region(int id, String name, Country country) {
		super();
		this.id = id;
		this.name = name;
		this.country = country;
	}

	public Region(int id, String name, Country country, int count) {
		super();
		this.id = id;
		this.name = name;
		this.country = country;
		this.count = count;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Country getCountry() { return country; }

	public void setCountry(int country_id) {
		this.country = country;
	}
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}	

}
