package com.myurbex.models;

import jakarta.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "country")
public class Country {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	@Basic
	@Column(name = "fr_name")
	private String fr_name;
	@Basic
	@Column(name = "en_name")
	private String en_name;
	@Basic
	@Column(name = "fr_capital_city")
	private String fr_capital_city;
	@Basic
	@Column(name = "en_capital_city")
	private String en_capital_city;

	@Transient
	private int count;
	
	public Country() {
		super();
	}
	
	public Country(int id, String fr_name, String en_name, String fr_capital_city, String en_capital_city) {
		super();
		this.id = id;
		this.fr_name = fr_name;
		this.en_name = en_name;
		this.fr_capital_city = fr_capital_city;
		this.en_capital_city = en_capital_city;
	}

	public Country(int id, String fr_name, String en_name, String fr_capital_city, String en_capital_city, int count) {
		super();
		this.id = id;
		this.fr_name = fr_name;
		this.en_name = en_name;
		this.fr_capital_city = fr_capital_city;
		this.en_capital_city = en_capital_city;
		this.count = count;
	}
	
	@Override
	public String toString() {
		return fr_name + " (" + count + ")";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFr_name() {
		return fr_name;
	}

	public void setFr_name(String fr_name) {
		this.fr_name = fr_name;
	}

	public String getEn_name() {
		return en_name;
	}

	public void setEn_name(String en_name) {
		this.en_name = en_name;
	}

	public String getFr_capital_city() {
		return fr_capital_city;
	}

	public void setFr_capital_city(String fr_capital_city) {
		this.fr_capital_city = fr_capital_city;
	}

	public String getEn_capital_city() {
		return en_capital_city;
	}

	public void setEn_capital_city(String en_capital_city) {
		this.en_capital_city = en_capital_city;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
}
