package com.myurbex.models;

import jakarta.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Basic
	@Column(name = "nickname")
	private String nickname;

	@Basic
	@Column(name = "email")
	private String email;

	@Basic
	@Column(name = "password")
	private String password;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_type_id", nullable = false)
	private User_Type userType;


	public User() {
		super();
	}

	public User(int id, String nickname, String email, String password) {
		super();
		this.id = id;
		this.nickname = nickname;
		this.email = email;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String name) {
		this.nickname = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User_Type getUserType() {
		return userType;
	}

	public void setUserType(User_Type userType) {
		this.userType = userType;
	}

	@Override
	public String toString(){
        return "Pseudo : "  + this.nickname + " ; email : " + this.email + " ; type user : " + this.getUserType().getLabel();
    }
}
