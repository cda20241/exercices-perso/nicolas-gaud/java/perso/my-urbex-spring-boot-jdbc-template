package com.myurbex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyUrbexApplication {
	public static void main(String[] args) {
		SpringApplication.run(MyUrbexApplication.class, args);
	}
}
