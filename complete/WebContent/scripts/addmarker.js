function loadAddMarker(){
	loadTypesAddMarker();
	loadStatusesAddMarker();
	loadMotifsAddMarker();
	loadCountriesAddMarker();
}

function loadModifyMarker(type_label, status_label, motif_label, country_id, region_id, department_id){
	loadTypesModifyMarker(type_label);
	loadStatusesModifyMarker(status_label);
	loadMotifsModifyMarker(motif_label);
	loadCountriesModifyMarker(country_id);
	loadRegionsModifyMarker(country_id, region_id);
	loadDeptsModifyMarker(country_id, region_id, department_id);
}

function toggleSelect(id, opt){
	var select = document.getElementById(id);	
	if (opt == 'showSelect'){
		select.classList.replace('hide', 'showSelect');
		document.getElementById('addRegions').setAttribute('required', '');
		document.getElementById('addDepts').setAttribute('required', '');
	}
	else if (opt == 'hide') {
		select.classList.replace('showSelect', 'hide');
		if (id == 'divRegions'){
			$('#addRegions').empty();
			document.getElementById('addRegions').removeAttribute('required');
			$('#addDepts').empty();
			document.getElementById('addDepts').removeAttribute('required');
		}
		
	} 
}

function loadTypesAddMarker(){
	$.ajax({
	    url:'PopulateTypes',
	    type:'GET',
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = "<option value=''>--- Selectionner ---</option>"; 	        
	        $.each(json, function(key, data) { 	    
	           HTML += "<option value='" + data.id + "'>" + data.label + "</option>"
	        });
	        $('#addTypes').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
}

function loadTypesModifyMarker(type_label){
	$.ajax({
	    url:'PopulateTypes',
	    type:'GET',
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = "<option value=''>--- Selectionner ---</option>"; 	        
	        $.each(json, function(key, data) { 	   	        	
	           HTML += "<option value='" + data.id + "'"
	           if (data.label == type_label){
	        	   HTML += " selected>"
	           }
	           else{
	        	   HTML += ">"
	           }
	           HTML += data.label + "</option>"
	        });
	        $('#addTypes').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
}

function loadStatusesAddMarker(){
	$.ajax({
	    url:'PopulateStatus',
	    type:'GET',
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = "<option value=''>--- Selectionner ---</option>"; 	        
	        $.each(json, function(key, data) { 	    
	           HTML += "<option value='" + data.id + "'>" + data.label + "</option>"
	        });
	        $('#addStatuses').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
}

function loadStatusesModifyMarker(status_label){
	$.ajax({
	    url:'PopulateStatus',
	    type:'GET',
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = "<option value=''>--- Selectionner ---</option>"; 	        
	        $.each(json, function(key, data) { 	    
	           HTML += "<option value='" + data.id + "'"
	           if (data.label == status_label){
	        	   HTML += " selected>"
	           }
	           else{
	        	   HTML += ">"
	           }
	           HTML += data.label + "</option>"
	        });
	        $('#addStatuses').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
}

function loadMotifsAddMarker(){
	$.ajax({
	    url:'PopulateMotifs',
	    type:'GET',
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = "<option value=''>--- Selectionner ---</option>"; 	        
	        $.each(json, function(key, data) { 	    
	           HTML += "<option value='" + data.id + "'>" + data.label + "</option>"
	        });
	        $('#addMotifs').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
}

function loadMotifsModifyMarker(motif_label){
	$.ajax({
	    url:'PopulateMotifs',
	    type:'GET',
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = "<option value=''>--- Selectionner ---</option>"; 	        
	        $.each(json, function(key, data) { 	    
	           HTML += "<option value='" + data.id + "'"
	           if (data.label == motif_label){
	        	   HTML += " selected>"
	           }
	           else{
	        	   HTML += ">"
	           }
	           HTML += data.label + "</option>"
	        });
	        $('#addMotifs').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
}

function loadCountriesAddMarker(){
	$.ajax({
	    url:'PopulateCountries',
	    type:'GET',
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = "<option value=''>--- Selectionner ---</option>"; 	        
	        $.each(json, function(key, data) { 	    
	           HTML += "<option value='" + data.id + "'>" + data.fr_name + "</option>"
	        });
	        $('#addCountries').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
}

function loadCountriesModifyMarker(country_id){
	$.ajax({
	    url:'PopulateCountries',
	    type:'GET',
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = "<option value=''>--- Selectionner ---</option>"; 	        
	        $.each(json, function(key, data) { 	    
	           HTML += "<option value='" + data.id + "'"
	           if (data.id == country_id){
	        	   HTML += " selected>"
	           }
	           else{
	        	   HTML += ">"
	           }
	           HTML += data.fr_name + "</option>"
	        });
	        $('#addCountries').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
}

function loadRegionsAddMarker(countryId){
	if (countryId == 150){
		$.ajax({
		    url:'PopulateRegions',
		    type:'GET',
		    dataType: 'json',
		    success: function( json ) {
		        var HTML = "<option value=''>--- Selectionner ---</option>"; 	        
		        $.each(json, function(key, data) { 	    
		           HTML += "<option value='" + data.id + "'>" + data.name + "</option>"
		        });
		        $('#addRegions').empty();
		        $('#addRegions').append(HTML); 
		    },
	        error : function(e) {
	            
	        }
		  });
		  toggleSelect('divRegions', 'showSelect');
	}
	else{
		 toggleSelect('divRegions', 'hide');
		 toggleSelect('divDepts', 'hide');
	}
}

function loadRegionsModifyMarker(countryId, regionId){
	if (countryId == 150){
		$.ajax({
		    url:'PopulateRegions',
		    type:'GET',
		    dataType: 'json',
		    success: function( json ) {
		        var HTML = "<option value=''>--- Selectionner ---</option>"; 	        
		        $.each(json, function(key, data) { 	    
			           HTML += "<option value='" + data.id + "'"
			           if (data.id == regionId){
			        	   HTML += " selected>"
			           }
			           else{
			        	   HTML += ">"
			           }
			           HTML += data.name + "</option>"
		        });
		        $('#addRegions').empty();
		        $('#addRegions').append(HTML); 
		    },
	        error : function(e) {
	            
	        }
		  });
		  toggleSelect('divRegions', 'showSelect');
	}
	else{
		 toggleSelect('divRegions', 'hide');
		 toggleSelect('divDepts', 'hide');
	}
}

function loadDeptsAddMarker(regionId){
	$.ajax({
	    url:'PopulateDepartments',
	    type:'GET',
	    data: {
	    	regionId: regionId
	    },
	    dataType: 'json',
	    success: function( json ) {
	        var HTML = "<option value=''>--- Selectionner ---</option>"; 	        
	        $.each(json, function(key, data) { 	    
	           HTML += "<option value='" + data.id + "'>" + data.id + " - " + data.name + "</option>"
	        });
	        $('#addDepts').empty();
	        $('#addDepts').append(HTML); 
	    },
        error : function(e) {
            
        }
	  });
	  toggleSelect('divDepts', 'showSelect');
}

function loadDeptsModifyMarker(countryId, regionId, deptId){
	if (countryId == 150){
		$.ajax({
		    url:'PopulateDepartments',
		    type:'GET',
		    data: {
		    	regionId: regionId
		    },
		    dataType: 'json',
		    success: function( json ) {
		        var HTML = "<option value=''>--- Selectionner ---</option>"; 	        
		        $.each(json, function(key, data) { 	    
		           HTML += "<option value='" + data.id + "'"
		           if (data.id == deptId){
		        	   HTML += " selected>"
		           }
		           else{
		        	   HTML += ">"
		           }
		           HTML += data.name + "</option>"
		        });
		        $('#addDepts').empty();
		        $('#addDepts').append(HTML); 
		    },
	        error : function(e) {
	            
	        }
		  });
		  toggleSelect('divDepts', 'showSelect');
	}
}

function handleFiles(inputId){
	var input = document.getElementById(inputId);
	$("label[for='" + inputId + "']").empty();
	var divImg = '#' + inputId.substring(inputId.length - 3);	
	$(divImg).empty();
	for (i = 0; i < input.files.length; i++){
		$("label[for='" + inputId + "']").append(input.files[i].name);
		if (i != input.files.length - 1){
			$("label[for='" + inputId + "']").append(';');
		}
		var fReader = new FileReader();
		fReader.readAsDataURL(input.files[i]);
		fReader.onloadend = function(event){
			var img = document.createElement('img');	
			img.setAttribute('src',  event.target.result);
		    img.setAttribute('class', 'addMarkerThumb show')
		    $(divImg).append(img);
		}
	}
}
