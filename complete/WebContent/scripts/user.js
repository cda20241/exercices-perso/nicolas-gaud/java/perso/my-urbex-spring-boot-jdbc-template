function doConnect(){
    $.ajax({
	    url:'Connection',
	    type:'POST',
        dataType : 'json',
        data : {
        	email: $('#email').val(),
        	password: $('#password').val()
        },
        success: function( json ) {	         	
	           if (json.id != null){
	        	   $('#connectModal').css('display', 'none');
	        	   $('#userWelcome').append('Bienvenue ' + json.nickname + ' !');
	        	   $('#btnSearch').attr('onclick', 'goToCoords(' + json.user_type_id + ');');
	        	   initialize(json.user_type_id);        	   
	           }	           
	    },
        error : function(json) {  
        	$('#email').val('');
        	$('#password').val('');
        	$('#error').empty();
        	$('#error').append('Identifiant ou mot de passe erron\u00e9'); 
        }
    });
}

function disconnect(){
	$.ajax({
	    url:'Deconnection',
	    type:'POST'
    }).done(function(){
    	window.location.href="/MyUrbex";
    });
	
}

function toggleModal(id){	 
	var modal = document.getElementById(id);	
	if (modal.classList.contains('show')) {
		modal.classList.replace('show', 'hide');
	} 
	else if (modal.classList.contains('hide')) {
		modal.classList.replace('hide', 'show');
	}	
}

function sendPassword(){	
	toggleModal('mailConfirmed');
	$('#mailConfirmedRes').empty();
	$('#mailConfirmedRes').append('Un email contenant votre mot de passe a \u00e9t\u00e9 envoy\u00e9 \u00e0 : <br/><br/>' + $('#emailMdp').val()); 
	$.ajax({
	    url:'LostPassword',
	    type:'POST',
        data : {
        	email: $('#emailMdp').val()
        }
    });
	$('#emailMdp').val('');
	toggleModal('mailModal');
}

function updateUser(){
	$('#errorUpdate').empty();
	if ($('#nicknameUpdate').val() == ''){
		$('#errorUpdate').append('Le champ "Pseudo" ne peut pas \u00eatre vide.');
	}
	else if ($('#emailUpdate').val() == ''){
		$('#errorUpdate').append('Le champ "Adresse e-mail" ne peut pas \u00eatre vide.');
	}
	else if ($('#passwordUpdate').val() != '' && $('#passwordUpdate2').val() == ''){
		$('#errorUpdate').append('Le champ "Confirmation mot de passe" ne peut pas \u00eatre vide.');
	}
	else if ($('#passwordUpdate').val() != '' && ($('#passwordUpdate').val() != $('#passwordUpdate2').val())){
		$('#errorUpdate').append('Les mots de passe de sont pas identiques.');
	}
	else{		
		$.ajax({
		    url:'UpdateUser',
		    type:'POST',
	        data : {
	        	nickname: $('#nicknameUpdate').val(),
	        	email: $('#emailUpdate').val(),
	        	password: $('#passwordUpdate').val()
	        }
	    }).done(function(){	  
	    	toggleModal('updateConfirmed');
	    	$('#updateConfirmedRes').append('Vos informations ont bien \u00e9t\u00e9 modifi\u00e9es.');
	    });
	}
}