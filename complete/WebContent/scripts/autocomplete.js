var names = [];

function autoCompleteMap(user_type_id){
	$.ajax({
	    url:'PopulateNamesAutoComplete',
	    type:'GET',
	    data: {
	    	user_type_id: user_type_id
	    },
	    dataType: 'json',
	    success: function( json ) {	 
	        $.each(json, function(key, data) { 		        
	        	names.push(data);	        	
	        });	
	        $( "#searchMap" ).autocomplete({
			    source: names,
			    minLength: 2,
			    select: function( event, ui ) {
			    	goToMarker(ui.item.value);
			    	ui.item.value = '';
			    }
			  });
	    },
        error : function(e) {
            
        }	    
	  });
	return names;
}