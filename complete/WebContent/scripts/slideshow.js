var slideIndex = 1;
var thumbIndex = 0;
var thumbsAtOnce = 11;
var thumbsTab = [];

function getImages(id){
	$.ajax({
	    url:'MarkerImages',
	    type:'GET',
	    data: {
	    	id: id
	    },
	    dataType: 'json',
	    success: function( json ) {	    
	        var htmlThumbs = ""; 	
	        var htmlImages = "";
	        $.each(json, function(key, data) { 	 
	        	thumbsTab.push("<div><img id='th" + parseInt(key) + "' src='/imgs/" + data.name +  "' class='thumb' onclick='current(" + parseInt(key) + ")'></div>"); 
	        	htmlImages += "<div class='mySlides'><div class='numbertext'>" + parseInt(key + 1) + " / " + json.length + (data.image_type_id == 3 ? (" - Date : " + data.date_taken + " - Utilisateur : " + data.user_nickname) : "") + "</div><img src='/imgs/" + data.name + "'></div>"	 
	        });
	        $('#images').append(htmlImages); 	       
	        current(0);	       
	    },
        error : function(e) {
            
        }
	  });	
}

function current(n){
    if (n >= thumbsTab.length) {thumbIndex = 0; n = 0;}
    if (n < 0) {thumbIndex = thumbsTab.length - 1; n = thumbsTab.length - 1;}
	showThumbs(thumbIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  if (slides.length > 1){
	  $('#images').append("<a class='prev' onclick='current(" + parseInt(thumbIndex - 1) +")'>&#10094;</a>");
	  $('#images').append("<a class='next' onclick='current(" + parseInt(thumbIndex + 1) +")'>&#10095;</a>");
  }
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  slides[slideIndex].style.display = "flex";
} 

function showThumbs(n){	
	  if (thumbsTab.length > 1){
		  $('#images').append("<a class='prev' onclick='current(" + parseInt(thumbIndex - 1) +")'>&#10094;</a>");
		  $('#images').append("<a class='next' onclick='current(" + parseInt(thumbIndex + 1) +")'>&#10095;</a>");
	  }
	$('#thumbs').empty();
	if (thumbsTab.length <= 11){
		for (i = 0; i < thumbsTab.length; i++){
			$('#thumbs').append(thumbsTab[i]);
		}
	}
	else{		
		$('#thumbs').append("<a class='thprev' onclick='current(" + parseInt(thumbIndex - 1) +")'>&#10094;</a>");
		$('#thumbs').append("<a class='thnext' onclick='current(" + parseInt(thumbIndex + 1) +")'>&#10095;</a>");
		if(n == 0){
			for (i = thumbsTab.length - 5; i < thumbsTab.length; i++){
				$('#thumbs').append(thumbsTab[i]);
			}
			for (i = 0; i <= n + 5; i++){
				$('#thumbs').append(thumbsTab[i]);
			}
		}
		else if (n >= 1 && n <= 4){
			for (i = thumbsTab.length - (5 - n); i < thumbsTab.length; i++){
				$('#thumbs').append(thumbsTab[i]);
			}
			for (i = 0; i <= n - 1; i++){
				$('#thumbs').append(thumbsTab[i]);
			}
			for (i = n; i <= n + 5; i++){
				$('#thumbs').append(thumbsTab[i]);
			}
		}
		else if (n <= thumbsTab.length && n >= thumbsTab.length - 5){
			for (i = n - 5; i < thumbsTab.length; i++){
				$('#thumbs').append(thumbsTab[i]);
			}
			for (i = 0; i <= 5 - (thumbsTab.length - n); i++){
				$('#thumbs').append(thumbsTab[i]);
			}
		}
		else if ((n > 4) || (n < (thumbsTab.length - 5))){
			for (i = n - 5; i <= n + 5; i++){
				$('#thumbs').append(thumbsTab[i]);
			}
		}
	}
	var thumbs = document.getElementsByClassName("thumb");
	for (i = 0; i < thumbs.length; i++) {
		thumbs[i].className = thumbs[i].className.replace(" active", "");
	}
	document.getElementById('th'.concat(thumbIndex)).className += " active";
	var slides = document.getElementsByClassName("mySlides");
	  for (i = 0; i < slides.length; i++) {
	      slides[i].style.display = "none";
	  }
	  slides[thumbIndex].style.display = "flex";
}

function zoomIn(image){
	var element = document.getElementById("zoom");
	var imgRef = document.getElementById(image);
	var img = document.createElement('img');
	img.setAttribute('src', image);
	img.setAttribute('width', '300');
	img.setAttribute('height', '180');
	img.setAttribute('alt', '');
	element.appendChild(img);
	var position = $(imgRef).offset();
	var minusY = 20;
	if(position.top > 550)
		minusY = 100;
	$(element).css({position:"absolute", left:position.left-10,top:position.top-minusY});
	element.style.display = "inline-block";
	element.classList.add('change');	
}

function zoomOut() {
	  var element = document.getElementById("zoom");
	  while (element.firstChild) {
		  element.removeChild(element.firstChild);
		}
	  element.classList.remove('change');
}