<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>       
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>My Urbex - Modifier un lieu</title>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/animate.css"> 
	<link rel="stylesheet" href="css/custom.css"> 
	<link rel="stylesheet" href="css/master.css">        
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSehV5u6NJ7r3r8DlyHIvOom1gPITO7tQ&libraries=places"></script>
	<script src="scripts/jquery-ui.js"></script>
	<script src="scripts/sidebar.js"></script>
	<script src="scripts/scriptsMaps.js"></script>
	<script src="scripts/autocomplete.js"></script>
	<script src="scripts/user.js"></script>
	<script src="scripts/addmarker.js"></script>
</head>
<body onload="loadModifyMarker('${marker.type_label}', '${marker.status_label}', '${marker.motif_label}', '${marker.country_id}', '${marker.region_id}', '${marker.department_id}');">
	<script type="text/javascript">
		if ('${sessionUser}' == null || '${sessionUser}' == ''){
			document.location.href="/MyUrbex"	
		};
	</script>
	<div class="container-fluid" id="middle">
		<main class="ct" role="main">
			<div class="row">
				<div class="col-lg-12">
					<div id="title">
						<h1>Modifier un lieu</h1>
					</div>
					<section class="content">
						<h2>Remplir le formulaire pour modifier un lieu</h2>
						<div class="default">
							<form id="updateMarker" class="form-horizontal" role="form" method="POST" action="/MyUrbex/UpdateMarker" 
							enctype="multipart/form-data">
								<div class="alert alert-info show" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">�</span>
									</button>
									<strong>Attention : </strong>Les champs marqu�s d'une �toile (<span class="star"> * </span>) sont obligatoires.					
								</div><br><br>
								<div class="alert alert-info show" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">�</span>
									</button>
									<strong>Attention : </strong>Au moins une image (satellite) est obligatoire.					
								</div>
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-6">
										<div id="errorInsert" class="error"></div>    
									</div>
									<div class="col-md-3"></div>
								</div>
								<div class="row">
									<div class="col-md-3 field-label-responsive">
										<label for="latLng"><span class="star">* </span>Latitude et longitude : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<input class="form-control" type="text" id="latLng" value="${marker.latitude}, ${marker.longitude}" required>
												<input type="hidden" id="lat" name="addLat" value="${marker.latitude}">
												<input type="hidden" id="lng" name="addLng" value="${marker.longitude}">
												<input type="hidden" id="mid" name="mid" value="${marker.id}">
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>
								<div class="row">
									<div class="col-md-3 field-label-responsive">
										<label for="addTitle"><span class="star">* </span>Titre : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<input class="form-control" type="text" name="addTitle" id="addTitle" value="${marker.name}" required>
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>		
								<div class="row">
									<div class="col-md-3 field-label-responsive">
										<label for="addDesc">Description : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group has-danger">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<textarea class="form-control" name="addDesc" id="addDesc" rows="2">${marker.description}</textarea>												
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>	
								<div class="row">
									<div class="col-md-3 field-label-responsive">
										<label for="mapsLink"><span class="star">* </span>Lien Google Maps : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group has-danger">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<input class="form-control" type="text" name="mapsLink" id="mapsLink" value="${marker.maps_link}" required>
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>									
								<div class="row">
									<div class="col-md-3 field-label-responsive">
										<label><span class="star">* </span>Urbex ?</label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group has-danger">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												  <input type="radio" id="urbexYes" name="urbex" value="1" ${marker.is_urbex ? "checked" : ""} required>
  												  <label for="urbexYes">Oui</label>
												  <input type="radio" id="urbexNo" name="urbex" value="0" ${marker.is_urbex ? "" : "checked"}>
  												  <label for="urbexNo">Non</label>  												  
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>		
								<div class="row">
									<div class="col-md-3 field-label-responsive">
										<label for="addTypes"><span class="star">* </span>Type : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group has-danger">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<select class="custom-select" name="addTypes" id="addTypes" required></select>												  
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>	
								<div class="row">
									<div class="col-md-3 field-label-responsive">
										<label for="addStatuses"><span class="star">* </span>Status : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group has-danger">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<select class="custom-select" name="addStatuses" id="addStatuses" required></select>												  
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>	
								<div class="row">
									<div class="col-md-3 field-label-responsive">
										<label for="addMotifs">Motif : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group has-danger">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<select class="custom-select" name="addMotifs" id="addMotifs"></select>												  
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>	
								<div class="row">
									<div class="col-md-3 field-label-responsive">
										<label for="addCountries"><span class="star">* </span>Pays : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group has-danger">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<select class="custom-select" name="addCountries" id="addCountries" 
												onchange="loadRegionsAddMarker(this.value);" required></select>												  
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>	
								<div id="divRegions" class="row hide">
									<div class="col-md-3 field-label-responsive">
										<label for="addRegions"><span class="star">* </span>Region : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group has-danger">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<select class="custom-select" name="addRegions" id="addRegions" onchange="loadDeptsAddMarker(this.value);" ></select>												  
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>									
								<div id="divDepts" class="row hide">
									<div class="col-md-3 field-label-responsive">
										<label for="addDepts"><span class="star">* </span>Departement : </label>
									</div>
									<div class="col-md-6">										 
										<div class="form-group has-danger">
											<div class="input-group mb-2 mr-sm-2 mb-sm-0">
												<select class="custom-select" name="addDepts" id="addDepts"></select>												  
											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>	
<!-- 								<div class="row"> -->
<!-- 									<div class="col-md-3 field-label-responsive"> -->
<!-- 										<label><span class="star">* </span>Image satellite : </label> -->
<!-- 									</div> -->
<!-- 									<div class="col-md-6">										  -->
<!-- 										<div class="form-group"> -->
<!-- 											<div class="input-group mb-2 mr-sm-2 mb-sm-0"> -->
<!-- 												<span class="btn btn-file"> -->
<!--                         							Parcourir&hellip; <input name="imgsat" id="imgsat" type="file" required single accept=".jpeg,.jpg,.png" onchange="handleFiles('imgsat');"> -->
<!--                     							</span> -->
<!-- 												<label id ="labelImgSat" for="imgsat" class="form-control file">Choisir un fichier</label> -->
<!-- 											</div> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 									<div class="col-md-3"> -->
<!-- 										<div id="sat"> -->
<%-- 											<c:forEach items="${marker.images}" var="image" varStatus="status"> --%>
<%-- 												<c:if test="${image.image_type_id == 1}"> --%>
<%-- 													<img src="/imgs/${image.name}" class="addMarkerThumb show"> --%>
<%-- 												</c:if> --%>
<%-- 											</c:forEach> --%>
<!-- 										</div>										 -->
<!-- 									</div> -->
<!-- 								</div>	 -->
<!-- 								<div class="row"> -->
<!-- 									<div class="col-md-3 field-label-responsive"> -->
<!-- 										<label>Image Street View : </label> -->
<!-- 									</div> -->
<!-- 									<div class="col-md-6">										  -->
<!-- 										<div class="form-group"> -->
<!-- 											<div class="input-group mb-2 mr-sm-2 mb-sm-0"> -->
<!-- 												<span class="btn btn-file"> -->
<!--                         							Parcourir&hellip; <input name="imgstr" id="imgstr" type="file" single accept=".jpeg,.jpg,.png" onchange="handleFiles('imgstr');"> -->
<!--                     							</span> -->
<!-- 												<label id ="labelImgStr" for="imgstr" class="form-control file">Choisir un fichier</label> -->
<!-- 											</div> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 									<div class="col-md-3"> -->
<!-- 										<div id="str"> -->
<%-- 											<c:forEach items="${marker.images}" var="image" varStatus="status"> --%>
<%-- 												<c:if test="${image.image_type_id == 2}"> --%>
<%-- 													<img src="/imgs/${image.name}" class="addMarkerThumb show"> --%>
<%-- 												</c:if> --%>
<%-- 											</c:forEach> --%>
<!-- 										</div>										 -->
<!-- 									</div> -->
<!-- 								</div>		 -->
<!-- 								<div class="row"> -->
<!-- 									<div class="col-md-3 field-label-responsive"> -->
<!-- 										<label>Vos images : </label> -->
<!-- 									</div> -->
<!-- 									<div class="col-md-6">										  -->
<!-- 										<div class="form-group"> -->
<!-- 											<div class="input-group mb-2 mr-sm-2 mb-sm-0"> -->
<!-- 												<span class="btn btn-file"> -->
<!--                         							Parcourir&hellip; <input name="imgusr" id="imgusr" type="file" multiple accept=".jpeg,.jpg,.png" onchange="handleFiles('imgusr');"> -->
<!--                     							</span> -->
<!-- 												<label id ="labelImgUser" for="imgusr" class="form-control file">Choisir le(s) fichier(s)</label> -->
<!-- 											</div> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 									<div class="col-md-3"> -->
<!-- 										<div id="usr"> -->
<%-- 											<c:forEach items="${marker.images}" var="image" varStatus="status"> --%>
<%-- 												<c:if test="${image.image_type_id == 3}"> --%>
<%-- 													<img src="/imgs/${image.name}" class="addMarkerThumb show"> --%>
<%-- 												</c:if> --%>
<%-- 											</c:forEach> --%>
<!-- 										</div>																			 -->
<!-- 									</div> -->
<!-- 								</div>																																																			 -->
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-6">
										<button type="submit" class="btn btnLeft" >Modifier</button>
										<span class="btn btnRight" onclick="window.location.href='/MyUrbex';">Annuler</span>
									</div>
									<div class="col-md-3"></div>
								</div>								 
							</form>
						</div>
					</section>
				</div>
			</div>
		</main>
	</div>
</body>
</html>