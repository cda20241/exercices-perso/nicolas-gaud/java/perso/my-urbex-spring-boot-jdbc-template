<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
  <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/animate.css"> 
        <link rel="stylesheet" href="css/custom.css"> 
        <link rel="stylesheet" href="css/master.css">        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSehV5u6NJ7r3r8DlyHIvOom1gPITO7tQ&libraries=places"></script>
        <script src="scripts/jquery-ui.js"></script>
        <script src="scripts/sidebar.js"></script>
        <script src="scripts/scriptsMaps.js"></script>
        <script src="scripts/autocomplete.js"></script>
  </head>
  <body>
	  	<div><p id="count"></p></div>
	   	<button class="accordion">Filtrer par visite</button>
  		<div class="panel">
		  <br>	
	      <div id="visited"></div>
		</div>	
  		<button class="accordion">Filtrer par urbex / non-urbex</button>
  		<div class="panel">
		  <br>	
	      <div id="isurbex"></div>
		</div>
  		<button class="accordion">Filtrer par type</button>
  		<div class="panel">
	      <input type="button" class="btn select" value="Select all" onclick="checkUncheckAll('checkType', true)">
	      <input type="button" class="btn " value="Unselect all" onclick="checkUncheckAll('checkType', false)">	      
	      <div id="types"></div>		  
		</div>
		<button class="accordion">Filtrer par status</button>
  		<div class="panel">
	      <input type="button" class="btn select" value="Select all" onclick="checkUncheckAll('checkStatus', true)">
	      <input type="button" class="btn " value="Unselect all" onclick="checkUncheckAll('checkStatus', false)">
	      <div id="statuses"></div>		  
		</div>
		<button class="accordion">Filtrer par pays</button>
  		<div class="panel">
	      <input type="button" class="btn select" value="Select all" onclick="checkUncheckAll('checkCountry', true)">
	      <input type="button" class="btn" value="Unselect all" onclick="checkUncheckAll('checkCountry', false)">
	      <div id="countries"></div>		  
		</div>
		<button class="accordion">Filtrer par r�gion</button>
  		<div class="panel">
	      <input type="button" class="btn select" value="Select all" onclick="checkUncheckAll('checkRegion', true)">
	      <input type="button" class="btn" value="Unselect all" onclick="checkUncheckAll('checkRegion', false)">
	      <div id="regions"></div>		  
		</div>
		<button class="accordion">Filtrer par d�partment</button>
  		<div class="panel">
	      <input type="button" class="btn select" value="Select all" onclick="checkUncheckAll('checkDept', true)">
	      <input type="button" class="btn" value="Unselect all" onclick="checkUncheckAll('checkDept', false)">
	      <div id="depts"></div>		  
		</div><br><br>	
<!-- 		<input type="checkbox" value="Pinned" id="Pinned" name="Pinned" onchange="filter()"><label for="Pinned">Pinned</label> -->
	</body>
</html>