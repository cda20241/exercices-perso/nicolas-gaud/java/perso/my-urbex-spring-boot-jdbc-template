<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
  		<title>My Urbex - Liste des lieux</title>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/animate.css"> 
        <link rel="stylesheet" href="css/custom.css"> 
        <link rel="stylesheet" href="css/master.css">      
        <link rel="stylesheet" href="css/slideshow.css">  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCSehV5u6NJ7r3r8DlyHIvOom1gPITO7tQ&libraries=places"></script>
        <script src="scripts/jquery-ui.js"></script>
        <script src="scripts/modal.js"></script>
        <script src="scripts/scriptsMaps.js"></script>
        <script src="scripts/autocomplete.js"></script>
        <script src="scripts/slideshow.js"></script>
  </head>
  <body>
  	    <script type="text/javascript">
 	    	if ('${sessionUser}' == null || '${sessionUser}' == ''){
 	    		document.location.href="/MyUrbex"	
 	    	}
	    </script>
		<div class="container-fluid" id="middle">	
			<main class="ct list" role="main">
			<div id="zoom"></div>
				<div class="row">
					<div class="col-lg-3">
						<div id="title">
							<h1>Liste des lieux</h1><br><br>
							<h2>Nombre de lieux : ${countRecords}</h2>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="welcome pulse">${sessionUser != null ? 'Bienvenue '.concat(sessionUser.nickname).concat(' !') : ''}</div><br>
						<span class="white">L�gende : </span>
						<span class="green">   Visit�</span> | 
						<span class="ok">   Ok</span> | 
						<span class="incertain">   Incertain</span> | 
						<span class="dead">   Dead</span>
					</div>
					<div class="col-lg-3">
						<a href="allMap.jsp"">Retour � la carte</a><br>
						<c:if test="${countRecords != maxRecords}">
							<a href="/MyUrbex/populatelistmarkers?ids=">Liste compl�te</a>
						</c:if>
					</div>
				</div>
				<section class="content">
					<div class="list_container">
						<div class="container-fluid">						
							<c:forEach items="${markers}" var="marker" varStatus="status">								
								<c:if test="${(status.count-1) % 4 == 0 || status.count == 1}">
									<div class="row">
								</c:if>	
								<div class="col-md-3">
									<div class="list_element ${marker.nickname != null ? 'Green' : (marker.status_label)}">
										<a href="/MyUrbex/markerdetails?id=${marker.id}&page=${currentPage}" class="img">
										<img alt="" id="/imgs/${marker.first_image}" src="/imgs/${marker.first_image}" onmouseover="zoomIn('/imgs/${marker.first_image}')" onmouseout="zoomOut()"></a>
										<a href="/MyUrbex/markerdetails?id=${marker.id}&page=${currentPage}" class="${marker.nickname != null ? 'green' : (marker.status_label.toLowerCase())}">${marker.name}</a>
									</div>
								</div>
								<c:if test="${status.count % 4 == 0 || status.count == markers.size()}">
									</div>
								</c:if>								
							</c:forEach>							
							<nav aria-label="hidden">
								<c:if test="${countPages != 1}">
									<ul class="pagination pg-blue pagination-sm justify-content-center">
										<c:if test="${currentPage != 1}">
			        						<li class="page-item">
										      <a href="/MyUrbex/populatelistmarkers?page=${currentPage - 1}" class="page-link">Previous</a>
										    </li>
			    						</c:if>
			    						<c:forEach begin="1" end="${countPages}" var="i">
							                <c:choose>
							                    <c:when test="${currentPage eq i}">
							                        <li class="page-item active">
			      										<span class="page-link">${i}</span>
			    									</li>
							                    </c:when>
							                    <c:otherwise>
							                    	<li class="page-item">
							                        	<a href="/MyUrbex/populatelistmarkers?page=${i}" class="page-link">${i}</a>
							                        </li>
							                    </c:otherwise>
							                </c:choose>
							            </c:forEach>
							            <c:if test="${currentPage lt countPages}">
											<li class="page-item">
										      	<a href="/MyUrbex/populatelistmarkers?page=${currentPage + 1}" class="page-link">Next</a>
										    </li>					            
			    						</c:if>
		    						</ul>
		    					</c:if>	
							</nav>								
						</div>
					</div>
				</section>		
			</main>
		</div>
  </body>
</html>